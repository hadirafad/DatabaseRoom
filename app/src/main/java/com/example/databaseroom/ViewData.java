package com.example.databaseroom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ViewData extends AppCompatActivity {

    TextView tvName,tvEmail,tvPhno,tvUsername, tvPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);

        tvUsername=findViewById(R.id.tvUsername);
        tvPassword=findViewById(R.id.tvPassword);
        tvEmail=findViewById(R.id.tvMail);
        tvName=findViewById(R.id.tvName);
        tvPhno=findViewById(R.id.tvPhno);


        Intent intent = getIntent();

        String name = intent.getStringExtra("name");
        String email = intent.getStringExtra("email");
        String username = intent.getStringExtra("username");
        String password = intent.getStringExtra("password");
        String phone = intent.getStringExtra("phone");

        tvName.setText(name);
        tvPhno.setText(phone);
        tvEmail.setText(email);
        tvUsername.setText(username);
        tvPassword.setText(password);

    }
}