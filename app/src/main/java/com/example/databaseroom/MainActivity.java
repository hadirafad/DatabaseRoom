package com.example.databaseroom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText name,email,username,phone,password;
    Button btnSave;
    RoomDB database;
    List<MainData> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        phone = findViewById(R.id.phone);
        btnSave = findViewById(R.id.btnSave);

        database =RoomDB.getInstance(this);
        dataList=database.mainDao().getAll();


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sName=name.getText().toString().trim();
                String sPhone=phone.getText().toString().trim();
                String sUname=username.getText().toString().trim();
                String sPass= password.getText().toString().trim();
                String sEmail=email.getText().toString().trim();

                if(!isValidEmail(sEmail))
                {
                    email.requestFocus();
                    email.setError("Enter Correct Mail_ID ..!!");

                }
                else
                {

                    MainData data=new MainData();
                    data.setEmail(sEmail);
                    data.setName(sName);
                    data.setUsername(sUname);
                    data.setPassword(sPass);
                    data.setPhone(sPhone);
                    database.mainDao().insert(data);
                    name.setText("");
                    email.setText("");
                    username.setText("");
                    password.setText("");
                    phone.setText("");

                    dataList.clear();
                    //MainData d=dataList.get(0);
                    String iEmail=data.getEmail();
                    String iName=data.getName();
                    String iUsername=data.getUsername();
                    String iPassword=data.getPassword();
                    String iPhno=data.getPhone();

                    Intent myIntent = new Intent(MainActivity.this, ViewData.class);
                    myIntent.putExtra("name", iName);
                    myIntent.putExtra("email", iEmail);
                    myIntent.putExtra("username", iUsername);
                    myIntent.putExtra("password", iPassword);
                    myIntent.putExtra("phone", iPhno);
                    startActivity(myIntent);
                }
            }
        });

    }
    public final static boolean isValidEmail(CharSequence target)
    {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
